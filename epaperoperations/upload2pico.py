import requests
import base64
import json
from epaperoperations.functions import get_device_details
from epaperoperations.cli_args import args, root_dir


def device_info(id_: str, bearer: str):
    response = requests.get(
        url=f'https://new.mypicosign.com/api/info/{id_}',
        headers={
            'Accept': 'application/json',
            'Authorization': f'Bearer {bearer}'
        })
    return response


def device_post_img(id_: str, bearer: str, imgfn: str):
    with open(imgfn, 'rb') as img:
        # base64.b64encode returns a bytes instance,
        # so it's necessary to call decode to get a str
        img_b64 = base64.b64encode(img.read()).decode()
    response = requests.post(
        url=f'https://new.mypicosign.com/api/image/'
            f'{id_}?display=0&singleupdate=false&flashless=false',
        headers={'Content-type': 'application/json',
                 'Accept': 'application/json',
                 'Authorization': f'Bearer {bearer}'},
        data=json.dumps({'image': f'data:image/png;base64,{img_b64}'})
    )
    return response


def device_imgfile_upload(id_: str, bearer: str, imgfn: str):
    print(imgfn)
    with open(imgfn, 'rb') as img:
        print(img)
    response = requests.put(
        url=f'https://new.mypicosign.com/api/image/{id_}?'
            f'display=0&process=false&singleupdate=false&nodither=false&'
            f'whitebg=false&flashless=false',
        data=img.read(),
        headers={
            'Content-Type': 'multipart/form-data',
            'Accept': 'application/json',
            'Authorization': f'Bearer {bearer}'})
    return response


def upload2pico():
    device_details = get_device_details(
        details_path=root_dir / 'display_details.yml',
        device_key=args.device)
    info = device_info(id_=device_details["device_id"],
                       bearer=device_details["bearer"])
    print(f'Uploading {args.output} to: {info.json()}\n')
    post_response = device_post_img(
        id_=device_details["device_id"],
        bearer=device_details["bearer"],
        imgfn=args.imgdir / args.output)
    print(f'Upload response: {post_response.json()}')
