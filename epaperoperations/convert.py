from PIL import Image, ImageEnhance
from epaperoperations.functions import get_device_details
from epaperoperations.cli_args import args, root_dir


def convert2bw():
    device_details = get_device_details(
        details_path=root_dir / 'display_details.yml',
        device_key=args.device)
    dimensions = device_details.get('dimensions')
    print(dimensions)
    # dimensions_list = dimensions.split('x')
    # resize_cmd = f'mogrify -resize {dimensions}^ -gravity center -extent ' \
    #              f'{dimensions} {args.imgdir / args.input}'
    # subprocess.call(shlex.split(resize_cmd))
    with Image.open(args.imgdir / args.input) as im:
        brightness = ImageEnhance.Brightness(im)
        # im = brightness.enhance(2)
        # print(dimensions_list[0], int(dimensions_list[1]))
        # im = im.resize((1404, 1872))
        # im = im.resize((int(dimensions_list[0]), int(dimensions_list[1])))
        blackAndWhiteImage = im.convert("1")
        # blackAndWhiteImage.show()
        quantizedImage = im.quantize(colors=2, method=2)
        # print(quantizedImage.format, quantizedImage.size,
        # blackAndWhiteImage.mode)
        blackAndWhiteImage.save(args.imgdir / args.output, "BMP")
        print(f'{args.imgdir / args.input} converted to 1 bit BW image: '
              f'{args.imgdir / args.output}')
