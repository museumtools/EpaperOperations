import yaml


def get_device_details(details_path, device_key):
    with open(details_path, 'r') as details_f:
        device_details = yaml.load(details_f.read(), Loader=yaml.BaseLoader)
    device = device_details[device_key]
    return device
