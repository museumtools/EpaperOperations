from argparse import ArgumentParser, HelpFormatter, ArgumentDefaultsHelpFormatter
from pathlib import Path

parser = ArgumentParser(
    description="""Epaper Operations""",
    formatter_class=ArgumentDefaultsHelpFormatter)

root_dir = Path('.').parent.parent

parser.add_argument('-d', '--device', default='7.8',
                    help='Device key in display_details.yml')
parser.add_argument('-a', '--action',
                    choices=['upload', 'convert', 'configure'],
                    default='convert', help=' ')
parser.add_argument('-i', '--input', default='image.png', help=' ')
parser.add_argument('-o', '--output', default='outputimg.data', help=' ')
parser.add_argument('-imgdir', '--imgdir', default=root_dir/'images', help=' ')

args = parser.parse_args()
