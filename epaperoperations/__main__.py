from epaperoperations.cli_args import args
from epaperoperations.convert import convert2bw
from epaperoperations.upload2pico import upload2pico


def main():
    print(args)
    print(args.device)
    print(args.imgdir / args.input)
    if args.action == 'convert':
        convert2bw()
    elif args.action == 'upload':
        upload2pico()


if __name__ == '__main__':
    main()
