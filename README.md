# CLI application to perform operations on [PicoSign](https://www.picosign.com/) ePaper devices

Application wraps PicoSign API requests - upload, configure - and image convertion into a simple cli.

Create for the [Museum Tools](https://museumtools.artserver.org/wiki/Main_Page) project 


## Install
`python setup.py install`

## Run
Information - *device_id*, *bearer*, *dimensions* - about of the PicoSign epaper device(s), 
against which the application is being run, should be stored devices needs to be stored 
in `display_details.yml` file. Use the `display_details.yml.template` as a 
template

**Upload:**

`epaper -a upload -d 7.8`

**Convert image**

`epaper -a convert`


**Getting help:**

`epaper --help`

```bash
usage: __main__.py [-h] [-d {7.8,13}] [-a {upload,convert,configure}]
                   [-i INPUT] [-o OUTPUT] [-imgdir IMGDIR]

Epaper Operations

optional arguments:
  -h, --help            show this help message and exit
  -d DEVICE, --device DEVICE
                        Device key in display_details.yml (default: 7.8)
  -a {upload,convert,configure}, --action {upload,convert,configure}
                        (default: convert)
  -i INPUT, --input INPUT
                        (default: image.png)
  -o OUTPUT, --output OUTPUT
                        (default: outputimg.data)
  -imgdir IMGDIR, --imgdir IMGDIR
                        (default: images)


```


André Castro 2020
