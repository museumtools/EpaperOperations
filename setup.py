from setuptools import setup
from pathlib import Path

README_path = Path.cwd() / 'README.md'
with open(README_path, 'r') as README_f:
    README = README_f.read()

setup(
    name='epaper operations',
    version='0.0.1',
    packages=['epaperoperations'],
    include_package_data=True,
    license='GPLv3',
    author='Andre Castro',
    author_email='andre.castro@tib.eu',
    long_description=README,
    long_description_content_type='text/markdown',
    classifiers=['Programming Language :: Python',
                 'Programming Language :: Python :: 3.7'],
    keywords='Semantic Mediawiki Ontology RDF SPARQL',
    install_requires=['requests', 'pyyaml', 'Pillow'],
    test_require=[],
    entry_points={
        'console_scripts': [
            'epaper = epaperoperations.__main__:main'
        ]
    })
